<?php

namespace App\Http\Controllers;

use App\SMPPServer\SMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\Array_;
use Symfony\Component\HttpFoundation\Response;
use App\User;
use Hash;
use App\SMPPServer\SmscApi;

class AuthController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10',
            'password' => 'required|min:6',
        ]);

        $user_check = User::where('phone_number', $request->phone_number)->first();

        if(!$user_check){
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 403);
        }

        $user_password_check = Hash::check($request->password, $user_check->password);

        if(!$user_password_check) {
            return response()->json([
                'status' => false,
                'error' => 'Неверный пароль!'
            ], 400);
        }

        $credentials = request(['phone_number', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([
                'error' => 'Пользователь не автаризован!'
            ], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Store a newly created user in the db.
     * 
     * @param   \Illuminate\Http\Request $request
     * @return  array
     */
    public function register(Request $request)
    {
        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10',
            'password' => 'required|min:6',
        ]);

        $user_check = User::where('phone_number', $request->phone_number)->first();

        if($user_check){
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером уже существует!'
            ], 400);
        }

        $user = new User();
        $user->phone_number = $request->phone_number;
        $user->password = Hash::make($request->password);
        $user->verification_code = mt_rand(1000, 9999);
        $user->save();

        // Вызываем класс по работе с СМС
        $sendSMS = new SmscApi();

        // Передаем параметры для отправки СМС
        $sendSMS->send_sms("7".$user->phone_number, "Verification code: " . $user->verification_code);

        return response()->json([ 
            'status' => true,
            'verification_code' => $user->verification_code
        ], 200);
    }

    // Переотправка кода подтверждения по смс пользователю
    public function resendVerificationCode(Request $request){
        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10'
        ]);

        $user_check = User::where('phone_number', $request->phone_number)->first();

        if(!$user_check){
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 400);
        }

        $user_check->verification_code = mt_rand(1000, 9999);
        $user_check->save();

        // Вызываем класс по работе с СМС
        $sendSMS = new SmscApi();

        $country_code = "99";

        // Передаем параметры для отправки СМС
        $sendSMS->send_sms("7".$user_check->phone_number, "Verification code: " . $user_check->verification_code);

        return response()->json([
            'status' => true,
            'verification_code' => $user_check->verification_code
        ], 200);

    }

    // Check phone number to send verify_code
    public function restorePassword(Request $request){
        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10',
        ]);

        $user_check = User::where('phone_number', $request->phone_number)->first();

        if(!$user_check) {
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 400);
        }

        if($user_check->verified !== 1 && $user_check->status !== 1){
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 400);
        }


        $user_check->verified = 0;
        $user_check->verification_code = mt_rand(1000, 9999);
        $user_check->save();

        // Вызываем класс по работе с СМС
        $sendSMS = new SmscApi();

        $country_code = "7";

        // Передаем параметры для отправки СМС
        $sendSMS->send_sms("7".$user_check->phone_number, "Verification code: " . $user_check->verification_code);

        return response()->json([
            'status' => true,
            'verification_code' => $user_check->verification_code
        ], 200);
    }

    // Check verify_code for reset password
    public function verifyRestorePassword(Request $request){
        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10',
            'verify_restore_code' => 'required|min:4|max:4',
        ]);

        $user_verify = User::where('phone_number', $request->phone_number)->first();

        if(!$user_verify){
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 400);
        }

        if($user_verify->verification_code !== $request->verify_restore_code) {
            return response()->json([
                'status' => false,
                'error' => 'Введен неверный код подтверждения!'
            ], 403);
        }

        $user_verify->verified = 1;
        $user_verify->save();

        return response()->json([
            'status' => true,
            'message' => 'Номер телефона успешно подтвержден!'
        ], 200);
    }

    // Generate new password
    public function generateNewPassword(Request $request){
        // Make validate data
        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10',
            'new_password' => 'required|min:6',
        ]);

        // Check user to exists
        $user_check = User::where('phone_number', $request->phone_number)->first();

        // If user not found return false
        if(!$user_check){
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 400);
        }

        // Else save new password
        $user_check->password = Hash::make($request->new_password);
        $user_check->verification_code = null;
        $user_check->save();

        return response()->json([
            'status' => true,
            'message' => 'Новый пароль успешно сохранён!'
        ], 200);

    }

    /**
     * Verify user by verification code.
     * 
     * @param int $verificationCode
     * @return array
     */
    public function verify(Request $request)
    {

        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10',
            'verify_code' => 'required|min:4|max:4',
        ]);

        $user_verify = User::where('phone_number', $request->phone_number)->first();

        if(!$user_verify){
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 400);
        }
        
        if($user_verify->verification_code !== $request->verify_code) {
            return response()->json([
                'status' => false,
                'error' => 'Введен неверный код подтверждения!'
            ], 403);
        }

        $user_verify->verified = 1;
        $user_verify->save();

        return response()->json([
            'status' => true,
            'message' => 'Номер телефона успешно подтвержден!'
        ], 200);
    }

    /**
     * Update user info after registration.
     * 
     * @param   \Illuminate\Http\Request $request
     * @return  array
     */
    public function completeRegister(Request $request)
    {
        $validated = $request->validate([
            'phone_number' => 'required|max:10|min:10',
            'email' => 'email|max:60|nullable',
            'pin_code' => 'required|confirmed|min:4|max:4'
        ]);

        $user_check = User::where('phone_number', $request->phone_number)->first();

        if(!$user_check) {
            return response()->json([
                'status' => false,
                'error' => 'Пользователь с таким номером не существует!'
            ], 403);
        }

        if(!$user_check->verified) {
            return response()->json([
                'status' => false,
                'error' => 'Сначала подтвердите номер телефона!'
            ], 403);
        }

        if(!empty($request->email)){
            $user_check->email = $request->email;
        }
        $user_check->pin_code = Hash::make($request->pin_code);
        $user_check->status = 1;
        $user_check->verification_code = null;
        $user_check->save();

        // Get the token
        $token = auth()->login($user_check);

        return response()->json([
            'status' => true,
            'message' => 'Данные пользователя успешно сохранены!',
            'user' => $user_check,
            'token' => $token
        ], 200);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' =>auth()->user()
        ]);
    }

    public function payload(){
        return auth()->payload();
    }
}
