<?php


Route::group(['prefix' => 'auth'], function () {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('payload', 'AuthController@payload');
    Route::post('register', 'AuthController@register');
    Route::post('restorePassword', 'AuthController@restorePassword');

    Route::put('completeRegister', 'AuthController@completeRegister');
    Route::put('verify', 'AuthController@verify');
    Route::put('resendVerificationCode', 'AuthController@resendVerificationCode');
    Route::put('restorePassword', 'AuthController@restorePassword');
    Route::put('verifyRestorePassword', 'AuthController@verifyRestorePassword');
    Route::put('generateNewPassword', 'AuthController@generateNewPassword');

});
