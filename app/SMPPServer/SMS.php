<?php

namespace App\SMPPServer;

class SMS
{
    public static function sendConfirmCode($to, $code)
    {
        if ($to && $code) {
            $sms = new SMPP();
            $from = 'Crypto';
            $message = 'Код подтверждения: ' . $code;
            $status = $sms->send_sms($from, $to, $message, 1);
            return $status;
        } else {
            return false;
        }
    }

    public static function sendSMS($to, $message)
    {
        if ($to && $message) {
            $sms = new SMPP();
            $from = 'Crypto';
            $status = $sms->send_sms($from, $to, $message, 1);
            return $status;
        } else {
            return false;
        }
    }

}